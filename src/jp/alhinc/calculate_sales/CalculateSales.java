package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_SERIAL_NUMBERS = "売上ファイル名が連番になっていません";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String BRANCH_NUMBERS_IS_INVALID = "の支店コードが不正です";
	private static final String COMMODITY_NUMBERS_IS_INVALID = "の商品コードが不正です";
	private static final String SALESAMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String FORMAT_IS_INVALID = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String>commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long>commoditySales = new HashMap<>();

		//コマンド引数チェック
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}+$", "支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$", "商品")) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1、2-2,商品売上ファイル)
		File[] files = new File(args[0]).listFiles();
		List <File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {

		//ファイルの桁数が数字8桁.rcdの場合
		if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")){
			rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);

		//連番チェック
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former != 1)) {
				System.out.println(FILE_NOT_SERIAL_NUMBERS);
				return;
			}
		}
		//売上ファイル読込
		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {

			try {
				List<String> rcdFilesValue = new ArrayList<>();
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;

				while((line = br.readLine()) != null) {
					//コード＆売上額をlistに保持させる
					rcdFilesValue.add(line);
				}

				String rcdFileNames = rcdFiles.get(i).getName();

				//売上ファイルの行数をチェック
				if(rcdFilesValue.size() != 3) {
					System.out.println(rcdFileNames + FORMAT_IS_INVALID);
					return;
				}

				//売上ファイルの支店コードの存在チェック
				if(!branchNames.containsKey(rcdFilesValue.get(0))) {
					System.out.println(rcdFileNames + BRANCH_NUMBERS_IS_INVALID);
					return;
				}
				//売上ファイルの商品コードの存在チェック
				if(!commodityNames.containsKey(rcdFilesValue.get(1))) {
					System.out.println(rcdFileNames + COMMODITY_NUMBERS_IS_INVALID);
					return;
				}

				//売上金額のデータが数字かチェック
				if(!rcdFilesValue.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//支店・商品別集計ファイルの売上加算
				long fileSales = Long.parseLong(rcdFilesValue.get(2));
				long branchSalesAmount = branchSales.get(rcdFilesValue.get(0)) + fileSales;
				long commoditySalesAmount = commoditySales.get(rcdFilesValue.get(1)) + fileSales;

				//支店ファイル売上金額が10桁を超えた場合
				if(branchSalesAmount >= 10000000000L) {
					System.out.println(SALESAMOUNT_OVER);
					return;
				}
				//商品ファイル売上金額が10桁を超えた場合
				if(commoditySalesAmount >= 1000000000L) {
					System.out.println(SALESAMOUNT_OVER);
					return;
				}

				branchSales.put(rcdFilesValue.get(0), branchSalesAmount);
				commoditySales.put(rcdFilesValue.get(1), commoditySalesAmount);

			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店・商品名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String regexp, String existFile) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//ファイルがなければエラーを返す
			if(!file.exists()) {
				//支店・商品ファイルが存在しない
				System.out.println(existFile + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			while((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String items [] = line.split(",");

				//支店・商品定義ファイルフォーマットの条件
				if(items.length != 2 || (!items[0].matches(regexp))) {
					System.out.println(existFile + FILE_INVALID_FORMAT);
				}

				//Mapに格納させる
				Names.put(items[0], items[1] );
				Sales.put(items[0], 0L);


				System.out.println(line);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別・商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店・商品コードと支店・商品名を保持するMap
	 * @param 支店・商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);

			//書き込み
			bw = new BufferedWriter(new FileWriter(file));

			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				bw.newLine();
			}

		}catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
